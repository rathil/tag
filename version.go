package main

import "fmt"

type version struct {
	Major uint32
	Minor uint32
	Patch uint32
}

func (a version) String() string {
	return fmt.Sprintf(
		`v%d.%d.%d`,
		a.Major,
		a.Minor,
		a.Patch,
	)
}
