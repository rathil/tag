FROM golang:1.19-alpine3.17 as builder
WORKDIR /app
COPY . .
RUN go build -ldflags="-s -w" -installsuffix "static" -o /build .

FROM alpine:3.17
RUN apk add git
COPY --from=builder /build /usr/local/bin/tag