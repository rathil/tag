package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"time"
)

func main() {
	if err := (&cli.App{
		Name:  `Tag manager`,
		Usage: `Utility to simplify work with versions using git tags`,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    `silent`,
				Aliases: []string{`s`},
				Usage:   `Silent mode`,
			},
			&cli.BoolFlag{
				Name:    `major`,
				Aliases: []string{`1`},
				Usage:   `Increment major version number`,
			},
			&cli.BoolFlag{
				Name:    `minor`,
				Aliases: []string{`2`},
				Usage:   `Increment minor version number`,
			},
			&cli.BoolFlag{
				Name:    `patch`,
				Aliases: []string{`3`},
				Usage:   `Increment patch version number`,
			},
			&cli.BoolFlag{
				Name:    `disable-create-in-git`,
				Aliases: []string{`dg`},
				Usage:   `Disable the tag creation feature in GIT`,
			},
			&cli.BoolFlag{
				Name:    `push`,
				Aliases: []string{`p`},
				Usage:   `Auto push to origin remote`,
			},
			&cli.StringSliceFlag{
				Name:  `hook`,
				Usage: `Run hook before create tag`,
			},
		},
		Action:               action,
		EnableBashCompletion: true,
		Authors: []*cli.Author{{
			Name:  `Ievgenii Kyrychenko`,
			Email: `rathil@rathil.com`,
		}},
		Copyright: fmt.Sprintf(`© 2020-%d rathil`, time.Now().Year()),
	}).Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
