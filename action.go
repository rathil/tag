package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"github.com/urfave/cli/v2"
)

func action(
	c *cli.Context,
) error {
	dataStatus, err := exec.Command(`git`, `status`).CombinedOutput()
	if err != nil {
		return fmt.Errorf(`get status, err: %s`, err)
	}
	if !bytes.Contains(dataStatus, []byte(`nothing to commit`)) {
		fmt.Println(string(dataStatus))
		return nil
	}
	if data, err := exec.Command(`git`, `fetch`, `--tags`).CombinedOutput(); err != nil {
		return fmt.Errorf(`fetch git tags, out: %s, err: %w`, data, err)
	}
	data, err := exec.Command(`git`, `tag`, `-l`, `--sort`, `-version:refname`).CombinedOutput()
	if err != nil {
		return fmt.Errorf(`get git tags, out: %s, err: %w`, data, err)
	}

	vCurrent := version{}

	lastTag := strings.SplitN(string(data), "\n", 2)
	items := regexp.MustCompile(`^v?(\d+)\.(\d+)\.(\d+)`).FindStringSubmatch(lastTag[0])
	for i := 1; i < len(items); i++ {
		if items[i] != "" {
			field := reflect.ValueOf(&vCurrent).Elem().Field(i - 1)
			value, err := strconv.Atoi(items[i])
			if err != nil {
				continue
			}
			field.SetUint(uint64(value))
		}
	}
	silent := c.Bool(`silent`)
	major := c.Bool(`major`)
	minor := c.Bool(`minor`)
	patch := c.Bool(`patch`)

	if !silent {
		fmt.Printf("Current version: %s\n", vCurrent)
	}

	if !major && !minor && !patch {
		return nil
	}
	vNew := version{}
	switch {
	case major:
		vNew.Major = vCurrent.Major + 1
	case minor:
		vNew.Major = vCurrent.Major
		vNew.Minor = vCurrent.Minor + 1
	case patch:
		vNew.Major = vCurrent.Major
		vNew.Minor = vCurrent.Minor
		vNew.Patch = vCurrent.Patch + 1
	}

	if !silent {
		fmt.Print(`Next version: `)
	}
	fmt.Print(vNew.String())
	if !silent {
		fmt.Println()
	}

	for index, hook := range c.StringSlice(`hook`) {
		cmdHook := exec.Command(`sh`, `-c`, hook)
		cmdHook.Env = append(cmdHook.Env, os.Environ()...)
		cmdHook.Env = append(cmdHook.Env, fmt.Sprintf(`TAG_TAG=%s`, vNew.String()))
		cmdHook.Stdin = os.Stdin
		cmdHook.Stdout = os.Stdout
		cmdHook.Stderr = os.Stderr
		if err := cmdHook.Run(); err != nil {
			return fmt.Errorf(`execute %d hook, err: %s`, index+1, err)
		}
	}

	if !c.Bool(`disable-create-in-git`) {
		if data, err := exec.Command(`git`, `tag`, vNew.String()).CombinedOutput(); err != nil {
			return fmt.Errorf(`create git tag, out: %s, err: %w`, data, err)
		}
		if !silent {
			fmt.Printf("Tag %s was successfully created\n", vNew)
		}
	}
	if c.Bool(`push`) {
		if data, err := exec.Command(`git`, `push`, `--tags`, `origin`).CombinedOutput(); err != nil {
			return fmt.Errorf(`push git tags, out: %s, err: %w`, data, err)
		}
		if !silent {
			fmt.Printf("Tag %s was successfully pushed\n", vNew.String())
		}
	}
	return nil
}
